<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class ProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1, 100) as $index) {
            DB::table('projects')->insert([
                'progress' => $faker->numberBetween($min = 1, $max = 100),
                'name' => $faker->word,
                'expected_time' => $faker->time(),
                'description' => $faker->sentence(5),
                'creator_id' => $faker->numberBetween($min = 1, $max = 100),
                'manager_id' => $faker->numberBetween($min = 1, $max = 100),
                'department_id' => $faker->numberBetween($min = 1, $max = 3),
                'deadline' => $faker->dateTime(),
            ]);
        }
    }
}
