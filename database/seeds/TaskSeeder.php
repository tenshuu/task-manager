<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class TaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1, 100) as $index) {
            DB::table('tasks')->insert([
                'name' => $faker->word,
                'description' => $faker->sentence(5),
                'mark' => $faker->word,
                'expected_time' => $faker->time(),
                'fact_time' => $faker->time(),
                'link' => $faker->word,
                'link_text' => $faker->word,
                'priority' => $faker->numberBetween($min=1,$max=5),
                'user_id' => $faker->numberBetween($min=1,$max=100),
                'inspector_id' => $faker->numberBetween($min=1,$max=100),
                'creator_id' => $faker->numberBetween($min=1,$max=100),
                'deadline' => $faker->dateTime,
                'status' => $faker->numberBetween($min=1,$max=3),
                'project_id' => $faker->numberBetween($min=1,$max=100),
                'created_at' => $faker->dateTime,
                'updated_at' => $faker->dateTime,
            ]);
        }
    }
}
