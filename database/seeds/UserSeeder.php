<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1, 100) as $index) {
            DB::table('users')->insert([
                'name' => $faker->word,
                'email' => $faker->email,
                'password' => $faker->word,
                'role' => $faker->numberBetween($min = 1, $max = 3),
                'position' => $faker->randomElement(['Аналитик', 'Менеджер', 'Разработчик', 'Дизайнер','Тестировщик','Руководитель','Администратор']),
                'department_id' => $faker->numberBetween($min = 1, $max = 3),
            ]);
        }

    }
}
