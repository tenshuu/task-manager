<?php

namespace App;

use App\Project;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

/**
 * Class Task
 *
 * @package App
 * @property int $id
 * @property string $name
 *
 * @mixin \Eloquent
 */

class Task extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name',
        'description',
        'mark',
        'expected_time',
        'fact_time',
        'link',
        'link_text',
        'priority',
        'user_id',
        'inspector_id',
        'creator_id',
        'deadline',
        'status',
        'project_id',
        'created_at',
        'updated_at',
    ];

    public function rules()
    {
        return [
            'name' => 'required|string|min:1|max:200',
            'description' => 'string|max:500',
            'mark' => 'integer|max:6|regex:/(^[1-6]+)/i',
            'link' => 'string|max:255',
            'link_text' => 'string|max:255',
            'priority' => 'integer|max:5|regex:/(^[1-5]+)/i',
            'user_id' => 'integer|regex:/(^[0-9]+)/i',
            'inspector_id' => 'integer|regex:/(^[0-9]+)/i',
            'deadline' => 'date_format:"Y-m-d H:i:s"',
            'project_id' => 'integer|regex:/(^[0-9]+)/i',
            'fact_time' => array('regex:/^(\d{0,}):([0-5][0-9])$/'),
            'expected_time' => array('regex:/^(\d{0,}):([0-5][0-9])$/'),
        ];
    }

    public function createTask(Request $request)
    {
        try {
            auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
        $user_id = auth()->id();

        $validator = Validator::make($request->all(), $this->rules());
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        } else {
            $task = new Task();
            $task->fillTaskCreate($request, $task, $user_id);
            $task->save();

            $project = Project::query()
                ->find($request->get('project_id'));
            $project->increment('tasks_amount');

            if ((($request->input('name')) != null) and
                (($request->input('description')) != null) and
                (($request->input('mark')) != null) and
                (($request->input('expected_time')) != null) and
                (($request->input('fact_time')) != null) and
                (($request->input('link')) != null) and
                (($request->input('link_text')) != null) and
                (($request->input('priority')) != null) and
                (($request->input('user_id')) != null) and
                (($request->input('inspector_id')) != null) and
                (($request->input('deadline')) != null)){
                $user = User::query()
                    ->find($user_id);
                $user->increment('task_completed_achieve');
                #TODO прикрепить ачивку

            }

            return response()->json('OK', 200);
        }
    }

    public function fillTaskCreate(Request $request, Task $task, $user_id)
    {
        $task->fill(array(
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'mark'=> $request->input('mark'),
            'expected_time'=> $request->input('expected_time'),
            'fact_time' => $request->input('fact_time'),
            'link' => $request->input('link'),
            'link_text' => $request->input('link_text'),
            'priority' => $request->input('priority'),
            'user_id' => $request->input('user_id'),
            'inspector_id' => $request->input('inspector_id'),
            'creator_id' => $user_id,
            'deadline' => $request->input('deadline'),
            'project_id' => $request->input('project_id'),
            'created_at' => now(),
            'updated_at' => now(),
        ));
    }

    public function fillTaskUpdate(Request $request, Task $task)
    {
        if (($name = $request->get('name')) != null) {
            $task->name = $name;
            $task->updated_at = now();
        }
        if (($description = $request->get('description')) != null) {
            $task->description = $description;
            $task->updated_at = now();
        }
        if (($mark = $request->get('mark')) != null) {
            $task->mark = $mark;
            $task->updated_at = now();
        }
        if (($expected_time = $request->get('expected_time')) != null) {
            $task->expected_time = $expected_time;
            $task->updated_at = now();
        }
        if (($fact_time = $request->get('fact_time')) != null) {
            $task->fact_time = $fact_time;
            $task->updated_at = now();
        }
        if (($link = $request->get('link')) != null) {
            $task->link = $link;
            $task->updated_at = now();
        }
        if (($link_text = $request->get('link_text')) != null) {
            $task->link_text = $link_text;
            $task->updated_at = now();
        }
        if (($priority = $request->get('priority')) != null) {
            $task->priority = $priority;
            $task->updated_at = now();
        }
        if (($user_id = $request->get('user_id')) != null) {
            $task->user_id = $user_id;
            $task->updated_at = now();
        }
        if (($inspector_id = $request->get('inspector_id')) != null) {
            $task->inspector_id = $inspector_id;
            $task->updated_at = now();
        }
        if (($deadline = $request->get('deadline')) != null) {
            $task->deadline = $deadline;
            $task->updated_at = now();
        }
        if (($status = $request->get('status')) != null) {
            $task->status = $status;
            $task->updated_at = now();
            $task->save();
        }
    }

    public function updateTask(Request $request, Task $task)
    {
        try {
            auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }

        $validator = Validator::make($request->all(), $this->rules());
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        } else {

            if (($task->status == 4) and ($status = $request->get('status') != 4)) {
                $project = Project::query()
                    ->find($task->project_id);
                $project->decrement('done_tasks');
            }

            $task->fillTaskUpdate($request, $task);
            $task->save();

            if ($status = $request->get('status') == 4) {
                $project = Project::query()
                    ->find($task->project_id);
                $project->increment('done_tasks');
            }

            return response()->json('OK', 200);
        }
    }

}
