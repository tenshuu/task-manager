<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;

class ProjectController extends ApiController
{
    public function index()
    {
        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
        $user = auth()->user();
        $project = Project::query()
            ->where('user_id', '=', $user->id)
            ->orderBy('name')
            ->get();

        return $this->sendResponse($project, 'OK', 200);
    }

    public function getOne(Project $project)
    {
        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }

        $project = Project::query()
            ->where('id', '=', $project->id)
            ->get();

        return $this->sendResponse($project, 'OK', 200);
    }

    public function store(Request $request)
    {
        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }

        $project = new Project();
        $project = $project->createProject($request);
        $project->users()->attach($request->get('users'));
        return $this->sendResponse($project, 'OK', 200);
    }

    public function delete(Project $project)
    {
        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }

        $project = Project::query()
            ->where('id', '=', $project->id)
            ->delete();

        return $this->sendResponse($project, 'OK', 200);
    }

    public function update(Request $request, Project $project)
    {
        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }

        $project->updateProject($request, $project);
        return $this->sendResponse($project, 'OK', 200);
    }
}
