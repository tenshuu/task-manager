<?php

namespace App\Http\Controllers;

use App\Position;

class PositionController extends Controller
{
    public function show()
    {
        return Position::all();
    }

}
