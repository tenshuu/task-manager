<?php

namespace App\Http\Controllers;

use App\Project;
use Illuminate\Http\Request;
use App\Task;

class TaskController extends ApiController
{
    public function index(Request $request)
    {
        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }

        $task = Task::query()
            ->orderBy('name')
            ->where('project_id', '=', $request->input('project_id'))
            ->get();

        return $this->sendResponse($task, 'OK', 200);
    }

    public function getOne(Task $task)
    {
        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }

        $task = Task::query()
            ->where('id', '=', $task->id)
            ->get();

        return $this->sendResponse($task, 'OK', 200);
    }

    public function store(Request $request)
    {
        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }

        $task = new Task();
        $task = $task->createTask($request);

        return $this->sendResponse($task, 'OK', 200);
    }

    public function delete(Request $request)
    {
        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }

        $task = Task::query()
            ->where('id', '=', $request->get('task_id'))
            ->delete();

        return $this->sendResponse($task, 'OK', 200);
    }

    public function update(Request $request, Task $task)
    {
        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }

        $task->updateTask($request, $task);
        return $this->sendResponse($task, 'OK', 200);
    }
}
