<?php

namespace App\Http\Controllers;

use App\Traits\ApiResponse;

class ApiController extends Controller
{
    use ApiResponse;

    public function sendErrorNotFound($element = 'Object')
    {
        return $this->sendError($element . ': ' . 'Not Found', 404);
    }

    public function sendErrorNoAccess()
    {
        return $this->sendError('NoAccess.', 401);
    }

    public function sendAccess()
    {
        return $this->sendResponse(null, 'Access.', 200);
    }

}
