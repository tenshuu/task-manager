<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Mail;
use Illuminate\Notifications\Notifiable;
use App\Project;


class AuthController extends ApiController
{

    use Notifiable;

    public function index()
    {
        $users = User::query()
            ->orderBy('name')
            ->get();


        return $this->sendResponse($users, 'OK', 200);
    }


    public function authenticate(Request $request)
    {
        $credentials = $request->only('login', 'password');

        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 400);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        return response()->json(compact('token'));
    }

    public function getAuthenticatedUser()
    {
        try {

            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());

        }

        return response()->json(compact('user'));
    }

    public function me()
    {
        $user = auth()->user();
        $user->load('role');
        $user->load('status');
        return response()->json(compact('user'));
    }

    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }

    public function login(Request $request)
    {
        $credentials = request(['login', 'password']);
        $validator = Validator::make($request->all(), [
            'login' => 'required|string|max:255|regex:/(^\S+)@(\S+\.)(\S+\S+)/i',
            'password' => 'required|string|min:8|max:127|regex:/(^[A-Za-z!@#$%^&*0-9]+)/i'
        ]);

        if (User::where('login', '=', $request->get('login'))->exists()) {
            response()->json(['error' => 'Пользователь с таким e-mail не найден.'], 401);
        }

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        }
        if (!$token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Не удаётся войти. Неверный login или пароль.'], 401);
        }

        $user = User::where('login', $request->get('login'))->first();
        $user->load('role');
        $user->load('status');
        return response()->json(compact('user', 'token'), 201);
    }

    public function changePass(Request $request)
    {
        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }

        $validator = Validator::make($request->all(), [
            'newpassword' => 'required|string|min:8|max:127|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?\d)[a-zA-Z!@#$%^&*\d]{8,}$/',
            'password' => 'required|string|min:8|max:127|regex:/(^[A-Za-z!@#$%^&*0-9]+)/',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        } else {
            $email = $user->email;
            $password = $request->get('password');
            $credentials = ['login' => $email, 'password' => $password];

            if (!$token = auth()->attempt($credentials)) {
                return response()->json(['error' => 'Неверный пароль'], 400);
            } else {
                $user->updatePassword($request, $user);
                return response()->json(['message' => 'Пароль успешно обновлен']);
            }
        }
    }

    public function userRegister(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|min:1|max:60|regex:/(^(?=.*?[a-zA-ZА-ЯЁёЙйа-я])([a-zA-ZАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдеёжзийклмнопрстуфхцчшщъыьэюя’\-()\s]{1,60})$)/i',
            'role_id' => 'required|integer|regex:/(^[0-9]+)/i',
            'status_id' => 'required|integer|regex:/(^[0-9]+)/i',
            'family_id' => 'integer'
            'email' => 'unique:users|string|max:255|regex:/(^\S+)@(\S+\.)(\S+\S+)/i',
            'points' => 'required|integer',
            'login' => 'required|string|min:1|max:60|regex:/(^(?=.*?[a-zA-Z])([a-zA-Z’\-()\s]{1,60})$)/i',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        }
        $permitted_chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz!@#$%^&*0123456789';
        $password = substr(str_shuffle($permitted_chars), 0, 10);
        $name = $request->get('name');
        $email = $request->get('email');

        $user = User::create([
            'name' => $name,
            'position' => $request->get('position'),
            'email' => $email,
            'role' => $request->get('role'),
            'password' => Hash::make($password),
            'department_id' => $request->get('department'),
        ]);

        $data = array('name' => $name, 'email' => $email, 'pass' => $password);
        Mail::send('emails.mail', $data, function ($message) use ($name, $email) {
            $message->to($email, $name)
                ->subject('Регистрация в системе Smart-task');
            $message->from('smarttaskmanager1@gmail.com', 'Smart-task');
        });

        return response()->json(['message' => 'Письмо отправлено на почту']);
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'string|min:1|max:60|regex:/(^(?=.*?[a-zA-ZА-ЯЁёЙйа-я])([a-zA-ZАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдеёжзийклмнопрстуфхцчшщъыьэюя’\-()\s]{1,60})$)/i',
            'email' => 'unique:users|string|max:255|regex:/(^\S+)@(\S+\.)(\S+\S+)/i',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        }
        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }

        $name = $request->input('name');
        $email = $request->input('email');
        if ($name != null) {
            $user->name = $name;
        }
        if ($email != null) {
            $user->email = $email;
        }
        $user->save();
        return $this->sendResponse($user->load('department'), 'Информация профиля успешно изменена', 200);
    }

    public function recover(Request $request) {
        $validator = Validator::make($request->all(), [
            'email' => 'string|max:255|regex:/(^\S+)@(\S+\.)(\S+\S+)/i',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        }
        $email = $request->get('email');
        $user = User::where('email', '=', $email)->first();
        if ($user == null) {
            return response()->json(['error' => 'Пользователь с таким e-mail не зарегистрирован.'], 400);
        }

        $user->getCode($user);
        $code = $user->recover;
        $name = $user->name;
        $data = array('name' => $name, 'code' => $code, 'email' => $email);
        Mail::send('emails.reset', $data, function ($message) use ($name, $email, $code) {
            $message->to($email, $name)
                ->subject('Восстановление пароля');
            $message->from('smarttaskmanager1@gmail.com', 'Smart-task');
        });
        return response()->json(['message' => 'На ваш e-mail отправлено письмо с инструкцией по сбросу пароля.']);
    }

    public function recoverPass(Request $request) {
        $validator = Validator::make($request->all(), [
            'password' => 'required|string|min:8|max:127|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?\d)[a-zA-Z!@#$%^&*\d]{8,}$/',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        }

        $code = $request->get('code');
        $user = User::where('recover', '=', $code)->first();

        if ($user == null) {
            return response()->json(['error' => 'Неверная ссылка.'], 400);
        }

        $user->updatePass($request, $user);

        return response()->json('OK', 200);
    }

    public function getProjects() {
        $user_id = auth()->id();

        $projects = User::query()
            ->find($user_id)
            ->userProjects()
            ->orderBy('name')
            ->get();

        return $this->sendResponse($projects, 'OK', 200);
    }
}
