<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Contracts\Auth\CanResetPassword;

/**
 * Class User
 *
 * @package App
 * @property int $id
 * @property carbon $updated_at
 * @property carbon $created_at
 * @property string $name
 * @property string $surname
 * @property string $email
 * @property string $password
 * @property string $remember_token
 *
 * @mixin \Eloquent
 */

class User extends Authenticatable implements JWTSubject, CanResetPassword
{
    use Notifiable;

    public $timestamps = false;

    protected $fillable = [
        'name',
        'position',
        'email',
        'password',
        'role',
        'department_id'
    ];

    protected $hidden = [
        'password',
        'remember_token',
        'recover',
    ];

    public function department()
    {
        return $this->hasOne(Departments::class, 'id', 'department_id');
    }

    public function userProjects()
    {
        return $this->belongsToMany('App\Project', 'user_project', 'user_id', 'project_id');
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function fillUser(Request $request, User $user)
    {
        $user->fill(array(
            'password' => Hash::make($request->input('newpassword')),
        ));
    }

    public function updatePassword(Request $request, User $user) {
        $user->fillUser($request, $user);
        $user->save();
    }

    public function getCode(User $user) {
        $permitted_chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz!@#$%^&*0123456789';
        $code = substr(str_shuffle($permitted_chars), 0, 15);
        $user->recover = $code;
        $user->save();
    }

    public function updatePass(Request $request, User $user) {
        $user->password = Hash::make($request->get('password'));
        $user->recover = 0;
        $user->save();
    }
}
