<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Position
 *
 * @package App
 * @property int $id
 * @property string $name
 *
 * @mixin \Eloquent
 */

class Position extends Model
{
    public $timestamps = false;
}
