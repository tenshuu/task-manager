<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

/**
 * Class Project
 *
 * @package App
 * @property int $id
 * @property string $name
 *
 * @mixin \Eloquent
 */

class Project extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'progress',
        'tasks_amount',
        'name',
        'status',
        'expected_time',
        'description',
        'creator_id',
        'manager_id',
        'department_id',
        'deadline',
    ];

    public function users()
    {
        return $this->belongsToMany('App\User', 'user_project', 'project_id', 'user_id');
    }

    public function rules()
    {
        return [
            'name' => 'required|string|min:1|max:200',
            'description' => 'string|max:500',
            'department' => 'datetime',
            'status' => 'integer|max:1|regex:/(^[1-4]+)/i',
            'deadline' => 'date_format:"Y-m-d H:i:s"',
            'manager_id' => 'integer|max:1|regex:/(^[0-9]+)/i',
        ];
    }

    public function createProject(Request $request)
    {
        try {
            auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
        $user_id = auth()->id();

        $validator = Validator::make($request->all(), $this->rules());
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        } else {
            $project = new Project();
            $project->fillProject($request, $project, $user_id);
            $project->save();

            return response()->json('OK', 200);
        }
    }

    public function fillProject(Request $request, Project $project, $user_id)
    {
        $project->fill(array(
            'name' => $request->get('name'),
            'expected_time' => $request->get('expected_time'),
            'description' => $request->get('description'),
            'creator_id' => $user_id,
            'manager_id' => $request->get('manager_id'),
            'department_id' => $request->get('department_id'),
            'deadline' => $request->get('deadline'),
        ));
    }

    public function updateProject(Request $request, Project $project)
    {
        $validator = Validator::make($request->all(), $this->rules());
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        } else {
            $project->fillProject($request, $project);
            $project->save();
            return response()->json('OK', 200);
        }
    }
}

