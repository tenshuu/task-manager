<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
    Route::post('changepass', 'AuthController@changePass');
    Route::post('registration', 'AuthController@userRegister');
    Route::post('update','AuthController@update');
    Route::post('recover', 'AuthController@recover');
    Route::post('recoverpass', 'AuthController@recoverPass');
    Route::post('users', 'AuthController@index');
    Route::get('projects', 'AuthController@getProjects');
});

Route::group([
    'prefix' => 'position'
], function () {
    Route::get('show', 'PositionController@show');
});

Route::group([
    'prefix' => 'project'
], function () {
    Route::post('show', 'ProjectController@index');
    Route::post('create', 'ProjectController@store');
    Route::post('delete/{project}', 'ProjectController@delete');
    Route::post('archivate/{project}', 'ProjectController@archivate');
    Route::post('update/{project}', 'ProjectController@update');
});

Route::group([
    'prefix' => 'task'
], function () {
    Route::post('show', 'TaskController@index');
    Route::post('create', 'TaskController@store');
    Route::post('delete', 'TaskController@delete');
    Route::post('update/{task}', 'TaskController@update');
});
